from mnist import MNIST
import matplotlib.pyplot as plt
import numpy as np
import random
import argparse
from time import time
import math

def f(x):
    """sigmoid function"""
    return 1.0/(1.0 + np.exp(-x))

def fp(x):
    """sigmoid derivated function"""
    return f(x)*(1.0-f(x))

def layer(X, Wij):
    """return vectors Y and Pot for given input and layer"""
    Pot = np.matmul(Wij, X)
    return (f(Pot), Pot)

def network(Xj, Wih, Whj):
    """return prediction for given input and weights"""
    return np.argmax(layer(layer(Xj, Whj)[0], Wih)[0])

def perf(images, labels, Whj, Wih):
    """prediction performance score on an input set, for given network"""
    Err = 0.0
    for (image, digit) in zip(images, labels):
        Xi = np.array(image)/255.0
        Err += int(digit != network(Xi, Whj, Wih))
    return 1.0 - Err / len(images)

def init_weights(a, b):
    """return normalized random weight matrix"""
    W = np.random.uniform(low=-1, high=1, size=(a, b))
    for c in range(a):
        W[c, :] /= np.linalg.norm(W[c, :])
    return W

# load mnist data as global variables
mndata = MNIST('data')
train_images, train_labels = mndata.load_training()
test_images, test_labels = mndata.load_testing()

def generate_network(epsilon, target_err, step, nb_hidden):
    """generate network given epsilon and target performance score"""
    print(f"Generating network for epsilon={epsilon} and target_err={target_err} with step={step} and {nb_hidden} hidden neurons...")
    # initialize Weights for hidden and output layers
    t = time()
    Whj = init_weights(nb_hidden, 784)
    Wih = init_weights(10, nb_hidden)

    Err = math.inf
    count = 0
    err_table = []
    test_err_table = []
    while Err > target_err:
        for s in range(step):
            count += 1
            index = random.randrange(0, len(train_images))  # choose a random index
            image, digit = train_images[index], train_labels[index]
            # forward propagation
            Xj = np.array(image)/255.0
            Yd = np.array([int(i == digit) for i in range(10)])
            (Xh, Poth) = layer(Xj, Whj)
            (Xi, Poti) = layer(Xh, Wih)
            # calculate error
            Di = np.multiply(fp(Poti), Yd - Xi)
            Dh = np.multiply(fp(Poth), np.matmul(Wih.transpose(), Di))
            # backpropagation
            Wih += epsilon * np.outer(Di, Xh)
            Whj += epsilon * np.outer(Dh, Xj)
        # test performance
        # calculate global error on 99 random patterns
        indexes = random.choices(range(len(train_images)), k=99)
        Err = 0.0
        for image, digit in zip([train_images[i] for i in indexes], [train_labels[i] for i in indexes]):
            Yd = np.array([int(i == digit) for i in range(10)])
            (Xh, Poth) = layer(np.array(image)/255.0, Whj)
            (Xi, Poti) = layer(Xh, Wih)
            Di = np.multiply(fp(Poti), Yd - Xi)
            Err += sum(abs(Di))
        Err /= len(indexes)
        err_table.append(Err)
        # calculate intermediate score on test set
        # we are not training with the test set, only calculating our potential score
        test_perf = perf(test_images, test_labels, Wih, Whj)
        test_err_table.append(1 - test_perf)

        print(f"count={count} Err={Err*100:.1f}% perf={test_perf*100:.1f}%")
    t = time() - t
    print(f"network convergence in {count} steps and {t:.0f} seconds")
    return Whj, Wih, err_table, test_err_table

# main program

index = random.randrange(0, len(train_images))  # choose random index
image, label = train_images[index], train_labels[index]
print(f"digit: {label}", mndata.display(image))
#plt.imshow(np.array(image).reshape(28, 28), cmap=plt.get_cmap('gray'))
#plt.show()

parser = argparse.ArgumentParser(description="MNIST-trained digit recognition 2-layer neural network")
parser.add_argument('--epsilon', type=float, default=0.1, help='epsilon (default 0.1)')
parser.add_argument('--target', type=float,  default=0.01, help='target error (default 0.01)')
parser.add_argument('--step', type=int,  default=10000, help='steps for main loop (default 10000)')
parser.add_argument('--hidden', type=int,  default=100, help='size of hidden layer (default 100)')
args = parser.parse_args()

(Whj, Wih, err_table, test_err_table) = generate_network(args.epsilon, args.target, args.step, args.hidden)
# calculate performance on test base
train_perf = perf(train_images, train_labels, Wih, Whj)
print(f"train performance: {train_perf*100:.2f}%")
test_perf = perf(test_images, test_labels, Wih, Whj)
print(f"test performance: {test_perf*100:.2f}%")

plt.plot([x*args.step for x in range(1,len(err_table)+1)], err_table, 'r-h', label="Err")
plt.plot([x*args.step for x in range(1,len(test_err_table)+1)], test_err_table, 'g-o', label="test Error")

plt.xlabel("Itération")
plt.ylabel("% d'erreur réelle")
plt.legend()
plt.show()