from distutils.log import error
import matplotlib.pyplot as plt
import numpy as np
import random

# print graphical representation in terminal
def printPattern(X):
    for i in range(8):
        for j in range(6):
            print('*' if X[i*6+j] == 1 else '.', end = '')
        print()

# Read pattern file and return (content, value)
def readPattern(name):
    X = np.zeros(48)
    yd = -1
    with open(name) as f:
        i = 0
        while i < 48:
            c = f.read(1)
            if c == '.':
                X[i] = 0
                i += 1
            elif c == '*':
                X[i] = 1
                i += 1
            elif c != '':
                pass
            else:
                print("error input file : Missing X values")
                exit(1)
        while yd < 0:
            c = f.read(1)
            if c == '':
                print("error input file : no yd")
                exit(1)
            d = ord(c) - ord('0')
            if d in range(10):
                yd = d
    return (X, yd)

# Add noise to pattern
def noisePattern(X, level):
    Y = np.copy(X)
    for i in range(len(X)):
        Y[i] = 1 - Y[i] if random.random() < level else Y[i]
    return Y

# neuron function
# returns the guessed digit (0 or 1)
def neuron(X, theta, W):
    pot = np.dot (X, W, out = None) - theta
    return 0 if pot <= 0.5 else 1

# Main
theta = 0.5
epsilon = 0.01
models = [readPattern(f"pattern{k}.txt") for k in range(2)]
while True:
    err_table = []
    W = np.random.uniform(low=-1, high=1, size=(48))
    W /= np.linalg.norm(W)

    Err = 1
    count = 0
    while Err > 0.0:
        (X, Yd) = models[random.randint(0, 1)]
        print(X, Yd)
        # calculate forward propagation
        Y = neuron(X, theta, W)
        print(f"Y = {Y}, Yd = {Yd}")

        # calculate error
        err = Yd - Y
        print(f"err = {err}")

        # Now apply back propagation
        W += epsilon * err * X

        # calculate error for each neuron
        Err = sum([abs(p[1] - neuron(p[0], theta, W)) for p in models])
        err_table.append(Err)
        print(f"Err = {Err}")
        count += 1
    print(f"network convergence in {count} steps")
    # Generalization
    levels=100
    result = np.zeros([2, levels])
    for level in range(levels):
        for k in range(2):
            patterns = [noisePattern(models[k][0], level/100.) for i in range(50)]
            result[k, level] = sum([neuron(p, theta, W) != models[k][1] for p in patterns]) / len(patterns)
    print(result)
    plt.plot(result[0], label="k=0")
    plt.plot(result[1], label="k=1")
    plt.xlabel("% de bruit")
    plt.ylabel("% d'erreur")
    plt.legend()
    plt.show()
    plt.plot(range(1,count+1), err_table, 'g-o', label="Err")
    plt.xlabel("Itération")
    plt.ylabel("% d'erreur")
    plt.legend()
    plt.show()