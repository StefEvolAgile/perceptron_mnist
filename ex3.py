from distutils.log import error
import matplotlib.pyplot as plt
import numpy as np
import random

# print graphical representation in terminal
def printPattern(X):
    for i in range(8):
        for j in range(6):
            print('*' if X[i*6+j] == 1 else '.', end = '')
        print()

# Read pattern file and return (content, value)
def readPattern(name):
    X = np.zeros(48)
    digit = -1
    with open(name) as f:
        i = 0
        while i < 48:
            c = f.read(1)
            if c == '.':
                X[i] = 0
                i += 1
            elif c == '*':
                X[i] = 1
                i += 1
            elif c != '':
                pass
            else:
                print(f"error input file : Missing X values for {name}")
                exit(1)
        while digit < 0:
            c = f.read(1)
            if c == '':
                print("error input file : no yd")
                exit(1)
            d = ord(c) - ord('0')
            if d in range(10):
                digit = d
    Yd = np.array([int(i == digit) for i in range(10)])
    return (X, digit, Yd)

# Add noise to pattern
def noisePattern(X, level):
    Y = np.copy(X)
    for i in range(len(X)):
        Y[i] = 1 - Y[i] if random.random() < level else Y[i]
    return Y

# neuron function
# return the probability of matching
def neuron(X, theta, W):
    pot = np.dot (X, W, out = None) - theta
    return (0 if pot <= 0.5 else 1, pot)

# Main
theta = 0.5
epsilon = 0.01
models = [readPattern(f"pattern{k}.txt") for k in range(10)]

# for p in models:
#     print(p[1])
#     printPattern(p[0])

while True:
    err_table = []
    W = np.zeros((10, 48))
    for i in range(10):
        W[i, :] = np.random.uniform(low=-1, high=1, size=(48))
        W[i, :] /= np.linalg.norm(W)
    Err = 100
    count = 0
    while Err > 0.0:
        (X, digit, Yd) = models[random.randint(0, 9)]
        # calculate forward propagation
        Y = np.array([neuron(X, theta, W[i, :])[1] for i in range(10)])
        #print(f"Y = {Y}, digit={digit} Yd = {Yd}")

        # calculate error for each neuron
        err = Yd - Y
        # print(f"err = {err}")

        # Now apply back propagation for each neuron
        for i in range(10):
            W[i, :] += epsilon * err[i] * X

        # calculate global error
        Err = 0
        for i in range(10):
            (X, digit, Yd) = models[i]
            # calculate forward propagation
            Y = np.array([neuron(X, theta, W[i, :])[0] for i in range(10)])
            # print(f"count = {count} Y = {Y}, digit={digit} Yd = {Yd}")
            err = Yd - Y
            err2 = sum([abs(err[i]) for i in range(10)])
            Err += err2
            # print(f"i={i} err = {err}, Err={Err}")
            #print(f"count = {count} i={i} Err = {Err} err2={err2} err={err} {Yd} {Y}")
        err_table.append(Err)
        count += 1
    print(f"network convergence in {count} steps")
    # Generalization
    levels=50
    result = np.zeros([10, levels])
    for level in range(levels):
        for k in range(10):
            patterns = [noisePattern(models[k][0], level/100.) for i in range(50)]
            assert(models[k][1] == k)
            Err = 0
            for p in patterns:
                for i in range(10):
                    (X, digit, Yd) = (p, k, models[k][2])
                    # calculate forward propagation
                    Y = np.array([neuron(X, theta, W[i, :])[0] for i in range(10)])
                    # print(f"count = {count} Y = {Y}, digit={digit} Yd = {Yd}")
                    err = Yd - Y
                    err2 = sum([abs(err[i]) for i in range(10)])
                    Err += err2
            result[k, level] = Err / len(patterns)
    print(result)

    for i in range(10):
        plt.plot(result[i], label=f"k={i}")
    plt.xlabel("% de bruit")
    plt.ylabel("% d'erreur")
    plt.legend()
    plt.show()
    plt.plot(range(1,count+1), err_table, 'g-o', label="Err")
    plt.xlabel("Itération")
    plt.ylabel("% d'erreur")
    plt.legend()
    plt.show()